<?php 

require 'vendor/autoload.php';
use Mpociot\BotMan\BotManFactory;
use React\EventLoop\Factory;

class bot {
 public $loop = '';
 public $botman = '';
    function __construct() {
                $this->loop = Factory::create();
                $this->botman = BotManFactory::createForRTM([
                        'slack_token' => 'redacted'
                ], $this->loop);
    }

}
