<?php
class bothelp {
	public function handleHelp($x) {
        $array = [
            "date" => "Displays the Date", 
            "day" => "Displays the day of the week"
        ];
        //$comma_separated = implode("\n", $array);
        $x->reply("You can use the following commands: \n");
        foreach($array as $key => $value){
            $x->reply("{$key}: {$value}");
        }
		
	}
}
