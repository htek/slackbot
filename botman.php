<?php

include 'classes/botdate.php';
include 'classes/bot.php';
include 'classes/bothelp.php';

require 'vendor/autoload.php';

use Mpociot\BotMan\BotManFactory;
use React\EventLoop\Factory;

/* create new instance of bot from bot class */
$botobj = new bot();

/* calling new bot to perform a hear command and response based on keyword date */
$botobj->botman->hears('.date', function($bot) {
		$test = new botdate();
		$test->handleDate($bot);
}); 

/* calling bot to perform a hear command and response based on keyword day */
$botobj->botman->hears('.day', function($bot) {
		$testa = new botdate();
		$testa->handleDay($bot);
}); 

/* calling bot to perform a hear command and response based on keyword help */
$botobj->botman->hears('.help', function($bot) {
		$testb = new bothelp();
		$testb->handleHelp($bot);
}); 


/* bot is listening for input */
$botobj->loop->run();


